# git-backup

Backup one or more remote git repositories to your file
system. Specify the git repositories in a simple configuration file in
Python's
[ConfigParser](http://docs.python.org/library/configparser.html)
format. Each repository is backed up initially with `git mirror` and
then kept up to date with `git remote update`.

## Requirements

* Python 2.7+
* git 1.6+
* No password prompt when cloning the git repositories you want to back up

## Quick start

Clone the repository or download a zip package.

Then, create a configuration file named `.git-backup` in your home
directory containing one or more sections, where each section
describes names a repository and indicates the remote url of the
repository and the local path where you would like it backed up. The
section is used when reporting the backup status to you.

    [my-repo]
    url=git://github.com/username/my-repo.git
    dir=/home/username/backups/my-repo

Then run git-backup. If you cloned the repository, execute the module
directory:

    $ python /path/to/git-backup

If you downloaded a zip package instead, execute the zip file:

    $ python /path/to/git-backup.zip

## git-backup and passwords

git-backup does **not** save passwords for you. If the git repository
you want to back up is not hosted publically, then you need to store
your public key on the server where your repository is hosted. If you
have done this and it is ssh that is prompting you for a passphrase to
access your private key, you need to set up ssh-agent (or something
like it) to remember your private key passphrase.
    
## Command line options

* `--help`   : Show command line options
* `--config` : Use a different config file besides ~/.git-config
* `--man`    : Displays this readme file on the command line

## Detailed configuration guide

### Sections

Each repository you want to back up must be named by a section in the
`git-backup` configuration file. Sections begin on a new line and are
enclosed in square brackets, like this:

    [section-name]

The name can be anything you like, but typically the name of the
section will be the same as the name of the repository, minus the
*.git* extension if your repository has one. For example, if you want
to back up a repository named `my-repo-1.git` then usually you will
create a section like this:

    [my-repo-1]

Each repository section must define two entries named *url* and *dir*
(each entry is a `key=value` pair, see
[ConfigParser](http://docs.python.org/library/configparser.html) for a
syntax reference):

* `url`: location of remote repository
* `dir`: local directory where you want to store the backup

`git-backup` backs up the repository at *url* into local directory
*dir*, and keeps the local clone in *dir* in sync with the repository
at *url* on each subsequent run of `git-backup`. If the remote
repository moves, you must change *url*. If you want to change the
backup location, just change *dir*.

For example:

    [my-repo-1]
    url=git://github.com/username/my-repo-1.git
    dir=/home/username/backups/my-repo-1

### Substitutions

Because git-backup uses Python's ConfigParser module, the
configuration file supports subsitutions.

If you put backups in one central root directory, you can use
substitutions to factor that directory out into one variable in your
configuration file by placing it into the `DEFAULT` section.

    [DEFAULT]
    backup_root=/home/username/backups
    
    [my-repo-1]
    url=git://github.com/username/my-repo-1.git
    dir=%(backup_root)s/my-repo-2
    
    [my-repo-2]
    url=git://github.com/username/my-repo-2.git
    dir=%(backup_root)s/my-repo-2

Another useful convention to follow is to always name your sections
and backup directories by the same name as the remote repositories
(minus the `.git` suffix, if the remote repository has such a
suffix). Then, you can use the `__NAME__` substitution to further
standardize your configuration file:

    [DEFAULT]
    backup_root=/home/username/backups
    
    [my-repo-1]
    url=git://github.com/username/%(__NAME__)s.git
    dir=%(backup_root)s/%(__NAME__)s
    
    [my-repo-2]
    url=git://github.com/username/%(__NAME__)s.git
    dir=%(backup_root)s/%(__NAME__)s

### Notification

Notifications are printed after all backups have been
run. Notifications let you know which backups succeeded and which
failed.

#### Notification format

The status of each repository backup is printed on its own
line. Status lines are printed in the order that repositories appear
in the configuration file, and each line contains the section name
from the configuration file. If there is an error during backup, the
error will be printed as part of the notification.

    [SUCCESS] section-name: action
    [FAILURE] section-name: action (message)

#### Notification destination

By default, git-backup notifies you of the status of all backups by
outputting to stdout. You can customize where it directs its output
using the notification variable in the `DEFAULT` section of the
configuration file (in case this may be more convenient than
redirecting output as part of the command where you execute
git-backup).

To direct the notifications to a file, put the filename after a `>`
symbol:

    [DEFAULT]
    notification=>/path/to/git-backup-status

To pipe the notifications to the standard input of a runnable program,
put the filename of the program after a `|` symbol:

    [DEFAULT]
    notification=|tee git-backup-status

To pipe the notifications to standard output (the default), elide the
path after the `|` symbol:

    [DEFAULT]
    notification=|

If your shell supports it, you can build up a string of shell
operators in the notification, e.g.,

    [DEFAULT]
    notification=|tr 'a-z' 'A-Z' >/path/to/loud-backup-status
    
## Backup algorithm

Every time you run `git-backup`, it executes the following steps,

* if it cannot access the remote repository, it reports an error;
* otherwise, if the local directory is empty or does not exist, it
  clones the remote repository into the local directory;
* otherwise, if the local directory is a clone of the remote
  repository, it updates it to be in sync with the remote repository;
* otherwise, if the local directory contains a clone but it does not
  appear to be a clone of the specified remote repository, it reports
  an error.

## Automating your backups

This is not a required part of git-backup, just a suggested extension
for how you can plug git-backup into an existing solution to gain
additional protection for your data.

You can use git-backup to periodically back up remote git repositories
to your local machine as part of an automated backup
workflow. Repositories backed up to your local machine can then be
copied to off-site storage using an existing file- or block-level
backup solution. E.g.:

                 git-backup
     ________________|_________________
    /                                  \
    bitbucket, github ===> local machine ===> off-site
                           \_________________________/
                                         |
                             existing backup solution

Of course, if your repositories are hosted on bitbucket or github,
then they are already off-site, and those services offer some degree
of reliability. However, there is peace of mind in knowing that your
repositories are backed up with all your other files using whatever
strategy you have chosen. Plus, it can't hurt to have to have your
repositories in more places.

If your repositories are stored in a place with reliability you trust,
that obviates git-backup.
    
### Cron

If git-backup is executed by cron, and you rely on password-protected
ssh keys for logging into your git repositories, then you will have to
set up ssh-agent before running git-backup in the cron job. This
involves exporting the `SSH_AUTH_SOCK` and `SSH_AGENT_PID` variables
prior to executing `git-backup`.

Keychain makes this easy, because it puts the necessary variables in a
file, ready to be sourced by your shell. Let's say your home directory
is `/home/suzy` and your machine's hostname is `raven`. If you have
keychain and you want to run `git-backup` every night at 1am you would
add a line like this to your crontab:

    0 1 * * * source /home/suzy/.keychain/raven-sh >/dev/null && python /home/suzy/bin/git-backup.zip >/home/suzy/log/git-backup 2>&1

If you also have a nightly backup cron job, you will want to schedule
it after `git-backup`.

### Notification

If you are running git-backup periodically in the background, you may
want it to email you the status of each backup job using a
notification and an external script:

    [DEFAULT]
    notification=|~/bin/emailme.py --subject=git-backup
