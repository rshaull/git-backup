"""
Return an open file, pipe, or stdout in a context manager (allows use
in a with block). Part of git-backup.
"""

import subprocess, sys, os

class ResultsReporter(object):
    """
    Report notifications to the user via different mechanisms. Specify
    the mechanism in a string using one of the following formats:
    
    * send output to a file: >filename
    * send output to a pipe: |scriptname
    * send output to stdout: |
    
    ResultsReporter is suitable for use in a with block, so you can
    use it like this:
    
      with ResultsReporter('>foo') as f:
          print >>f, "a"
          print >>f, "b"
    
      with ResultsReporter('|emailer --to=me@email.com --subject=status') as p:
          print >>p, "foo bar baz"
    
    The purpose of ResultsReporter is to interoperate with git-backup,
    which collects notifications, one notification per repository per
    line, and then prints them to whatever output location (file,
    pipe, or stdout) is specified in the configuration file.
    """
    __slots__ = ('type', 'path', 'file')
    
    def __init__(self, notification):
        if not isinstance(notification, str):
            raise RuntimeError('Notification must be a string')
        if not (notification[0] == '|' or notification[0] == '>'):
            raise RuntimeError('Notification must begin with > or |')
        if notification[0] == '>' and len(notification[1:]) == 0:
            raise RuntimeError('Notification requires path after >')
        (self.type, self.path) = (notification[0], notification[1:])
    def __enter__(self):
        if len(self.path) == 0:
            # special case, send output to stdout
            self.file = sys.stdout
            return self.file
        if self.type == '>':
            # reporting to file
            self.file = open(os.path.expanduser(self.path), 'w')
            return self.file
        if self.type == '|':
            # reporting to pipe
            self.file = subprocess.Popen(
                self.path, shell=True, stdin=subprocess.PIPE).stdin
            return self.file
    def __exit__(self, type, value, traceback):
        # Only files get closed, not pipes and stdout
        if self.type == '>':
            self.file.close()
