"""
Actions that a python module can take against a git repository or clone.

Every action returns a Result which specifies whether or not the
action succeeded, a descriptive name for that action, and a message
(which will contain an error message if success==False).
"""

import os, subprocess
from result import Result
from enterdir import EnterDir

def _do_cmd(cmd):
    "Executes shell command cmd and returns tuple (exit_code, cmd_output)."
    try:
        return (0, subprocess.check_output(cmd, shell=True))
    except subprocess.CalledProcessError as error:
        return (error.returncode, error.output)

def _sanity_check(remote, local):
    """
    Check that the local clone appears to be a git repository and that
    it appears to be a clone of the remote repository. Does _not_
    check that the remote repository exists (use ping for that).
    """
    url = os.popen('git config --get remote.origin.url').read().strip()
    if len(url) == 0:
        (success, msg) = (False, '%s is not a git repository' % local)
    elif url != remote:
        (success, msg) = (False, '%s is not a git repository' % local)
    else:
        (success, msg) = (True, '')
    return Result(name='config', success=success, msg=msg)

def ping(remote):
    "Check that the remote repository is accessible."
    with open(os.devnull, 'w') as devnull:
        code = subprocess.call('git ls-remote %s' % remote,
                               stdout=devnull, stderr=devnull, shell=True)
        if code == 0:
            (success, msg) = (True, '')
        else:
            (success, msg) = (False, 'cannot reach repository')
        return Result(name='config', success=success, msg=msg)

def mirror(remote, local):
    """
    Mirrors remote git repository into empty directory local, creating
    the local path if necessary.
    """
    with EnterDir(local, create_if_not_exist=True):
        (code, output) = _do_cmd('git clone --mirror %s %s' % (remote, local))
        return Result(name='clone', success=(code==0), msg=output)

def update(remote, local):
    """
    Updates remote git repository into clone at the local directory.

    Before updating, performs sanity checks to make sure that local
    directory is a git repository and that it is a clone of the
    specified remote repository.
    """
    with EnterDir(local):
        result = _sanity_check(remote, local)
        if not result.success:
            return result
        (code, output) = _do_cmd('git remote update')
        return Result(name='update', success=(code==0), msg=output)
