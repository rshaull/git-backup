import unittest
import subprocess
import tempfile
import shutil
import os
from distutils.version import LooseVersion

"""
System test / regression test that operates git-backup via its
command-line interface, rather than via the internal Python function
APIs.

Sets up and manipulates git repositories under the current directory
(deleting them after the test). Will test that git-backup backs up the
data in the created repositories and that git-backup handles error
conditions correctly. The test will also fail if git is not found on
your path.
"""

######################################################################
# Helpers
######################################################################

def make_config_file(test_root):
    with open('%s/config' % test_root, 'w') as f:
        print >>f, """[repo1]
url=%(test_root)s/git/repo1
dir=%(test_root)s/bak/repo1

[repo2]
url=%(test_root)s/git/repo2
dir=%(test_root)s/bak/repo2""" % {'test_root': test_root}

def make_bad_config_file(test_root):
    "Make config file where both urls point to the same repo"
    with open('%s/config' % test_root, 'w') as f:
        print >>f, """[repo1]
url=%(test_root)s/git/repo1
dir=%(test_root)s/bak/repo1

[repo2]
url=%(test_root)s/git/repo1
dir=%(test_root)s/bak/repo2""" % {'test_root': test_root}

def init_repo(test_root, name):
    repo_path = '%s/git/%s' % (test_root, name)
    copy_path = '%s/working/%s' % (test_root, name)
    os.makedirs(copy_path)
    subprocess.check_output('git init', cwd=copy_path, shell=True)
    with open('%s/foo' % copy_path, 'w') as f:
        print >>f, 'data1'
    subprocess.check_output('git add foo', cwd=copy_path, shell=True)
    subprocess.check_output("git commit -m 'Adding foo'",
                            cwd=copy_path, shell=True)
    subprocess.check_output('git remote add origin %s' % repo_path,
                            cwd=copy_path, shell=True)
    subprocess.check_output('git push -q -u origin master',
                            cwd=copy_path, shell=True)

def make_repo(test_root, name):
    repo_path = '%s/git/%s' % (test_root, name)
    os.makedirs(repo_path)
    subprocess.check_output('git --bare init', cwd=repo_path, shell=True)
    init_repo(test_root, name)

def update_file(test_root, repo_name, data):
    repo_path = '%s/git/%s' % (test_root, repo_name)
    copy_path = '%s/working/%s' % (test_root, repo_name)
    with open('%s/foo' % copy_path, 'w') as f:
        print >>f, data
    subprocess.check_output('git add foo', cwd=copy_path, shell=True)
    subprocess.check_output("git commit -m 'Updating foo'",
                            cwd=copy_path, shell=True)
    subprocess.check_output('git push -q',
                            cwd=copy_path, shell=True)

def git_backup(test_runner, test_root):
    try:
        return subprocess.check_output('python . -c %s/config' % test_root,
                                       shell=True)
    except subprocess.CalledProcessError as error:
        test_runner.fail('Could not execute git-backup')

def check_backup(test_root, repo_name):
    """
    Return the contents of the file foo from the backup. Normally, you
    wouldn't clone the mirror made by git-backup, because you don't
    commit changes back to it. However, we clone it here just to make
    sure that it contains the data we expect it to. Deletes the local
    clone after getting the file contents.
    """
    bak_path="%s/bak/%s" % (test_root, repo_name)
    local_path="%s/local/%s" % (test_root, repo_name)
    os.makedirs(local_path)
    subprocess.check_output('git clone %s %s' % (bak_path, local_path),
                            shell=True)
    f = open('%s/foo' % local_path, 'r')
    s = f.read().strip()
    f.close()
    shutil.rmtree(local_path)
    return s

######################################################################
# Tests
######################################################################

class TestGit(unittest.TestCase):
    def test_run_git_and_check_version(self):
        try:
            s = subprocess.check_output('git --version', shell=True)
            version = s.split()[2] # s="git version a.b.c.d"
            self.assertTrue(LooseVersion(version) >= LooseVersion('1.6'),
                            'Requires git version >= 1.6')
        except subprocess.CalledProcessError as error:
            self.fail('Could not execute git')

class TestBasicFailures(unittest.TestCase):
    def setUp(self):
        self.test_root = tempfile.mkdtemp()
        make_config_file(self.test_root)
    def tearDown(self):
        shutil.rmtree(self.test_root)
    def test_cannot_find_any_repositories(self):
        lines = git_backup(self, self.test_root).splitlines()
        # The following are based on make_config_file
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[FAILURE\] repo1: config')
        self.assertRegexpMatches(lines[1], '^\[FAILURE\] repo2: config')

class TestBasicCloneAndUpdate(unittest.TestCase):
    def setUp(self):
        self.test_root = tempfile.mkdtemp()
        make_config_file(self.test_root)
    def tearDown(self):
        shutil.rmtree(self.test_root)
    def test_clone(self):
        make_repo(self.test_root, 'repo1')
        make_repo(self.test_root, 'repo2')
        lines = git_backup(self, self.test_root).splitlines()
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: clone')
        self.assertRegexpMatches(lines[1], '^\[SUCCESS\] repo2: clone')
    def test_update(self):
        make_repo(self.test_root, 'repo1')
        make_repo(self.test_root, 'repo2')
        # the SECOND git_backup should result in an update rather than a clone
        git_backup(self, self.test_root).splitlines()
        lines = git_backup(self, self.test_root).splitlines()
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: update')
        self.assertRegexpMatches(lines[1], '^\[SUCCESS\] repo2: update')

class TestMoreFailures(unittest.TestCase):
    def setUp(self):
        self.test_root = tempfile.mkdtemp()
        make_config_file(self.test_root)
    def tearDown(self):
        shutil.rmtree(self.test_root)
    def test_cannot_find_first_repository(self):
        make_repo(self.test_root, 'repo2')
        lines = git_backup(self, self.test_root).splitlines()
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[FAILURE\] repo1: config')
        self.assertRegexpMatches(lines[1], '^\[SUCCESS\] repo2: clone')
    def test_cannot_find_second_repository(self):
        make_repo(self.test_root, 'repo1')
        lines = git_backup(self, self.test_root).splitlines()
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: clone')
        self.assertRegexpMatches(lines[1], '^\[FAILURE\] repo2: config')
    def test_wrong_repository_url(self):
        make_repo(self.test_root, 'repo1')
        make_repo(self.test_root, 'repo2')
        # First we run git_backup; both should succeed as clones
        lines = git_backup(self, self.test_root).splitlines()
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: clone')
        self.assertRegexpMatches(lines[1], '^\[SUCCESS\] repo2: clone')
        # Now we replace the config with the bad config where repo2
        # has the same url as repo1, so repo2 should have a config
        # error
        make_bad_config_file(self.test_root)
        lines = git_backup(self, self.test_root).splitlines()
        self.assertEqual(len(lines), 2,
                         'Incorrect number of notification lines')
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: update')
        self.assertRegexpMatches(lines[1], '^\[FAILURE\] repo2: config')

class TestMoreUpdates(unittest.TestCase):
    def setUp(self):
        self.test_root = tempfile.mkdtemp()
        make_config_file(self.test_root)
        make_repo(self.test_root, 'repo1')
        make_repo(self.test_root, 'repo2')
    def tearDown(self):
        shutil.rmtree(self.test_root)
    def test_update(self):
        lines = git_backup(self, self.test_root).splitlines()
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: clone')
        self.assertEqual(check_backup(self.test_root, 'repo1'), 'data1')
        # Update repository, then update backup, then check backup contents
        update_file(self.test_root, 'repo1', 'data2')
        lines = git_backup(self, self.test_root).splitlines()
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: update')
        self.assertEqual(check_backup(self.test_root, 'repo1'), 'data2')
        # Again...
        update_file(self.test_root, 'repo1', 'data3')
        lines = git_backup(self, self.test_root).splitlines()
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: update')
        self.assertEqual(check_backup(self.test_root, 'repo1'), 'data3')
        # If we DON'T change the file, updating shouldn't change the result
        lines = git_backup(self, self.test_root).splitlines()
        self.assertRegexpMatches(lines[0], '^\[SUCCESS\] repo1: update')
        self.assertEqual(check_backup(self.test_root, 'repo1'), 'data3')
