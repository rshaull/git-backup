import sys, os, collections, subprocess, argparse
from ConfigParser import ConfigParser
from result import Result, if_failure, retry, result_to_s
from report import ResultsReporter
from gitactions import ping, mirror, update

######################################################################
# Defaults
######################################################################

default_config_file  = '~/.git-backup'
default_notification = '|' # stdout
default_max_attempts = '1' # no retries
default_backoff      = '5' # seconds

######################################################################
# Helpers
######################################################################

def refresh(remoteurl, clonedir):
    "clone or update clonedir from remoteurl, assuming remoteurl is valid"
    if not os.path.exists(clonedir):
        return mirror(remoteurl, clonedir)
    else:
        return update(remoteurl, clonedir)

def do_backup(remoteurl, clonedir):
    "Backup remoteurl into clonedir unless remoteurl is inaccessible"
    return if_failure(ping(remoteurl)) or refresh(remoteurl, clonedir)

######################################################################
# Parse arguments
######################################################################

parser = argparse.ArgumentParser(description='Backup git repositories')
parser.add_argument('-c', '--config', dest='config_file', type=str,
                    default=os.path.expanduser(default_config_file),
                    help='Path to configuration file (default: %(default)s)')
parser.add_argument('-m', '--man', dest='man', action='store_true',
                    help='Full manual page')
args = parser.parse_args()

if args.man:
    # Read manual in zip-safe way (works if module is invoked in zip package):
    from pkg_resources import resource_stream
    print resource_stream(__name__, 'readme.md').read()
    sys.exit(0)

######################################################################
# Parse config file
######################################################################

config = ConfigParser({'notification': default_notification,
                       'max_attempts': default_max_attempts,
                       'backoff'     : default_backoff},
                      collections.OrderedDict)
if not os.path.exists(args.config_file):
    raise RuntimeError('Cannot find config file %s' % args.config_file)
config.read([os.path.expanduser(args.config_file)])

######################################################################
# Perform backups and report results
######################################################################

results = collections.OrderedDict()
for section in config.sections():
    # paths in .git/config are stored as fully expanded paths, so we
    # need to expand local paths from the config file. Note that urls
    # beginning with file:// won't get expanded, but that's okay
    # because git disallows ~ in file:// urls.
    remoteurl = os.path.expanduser(config.get(section, 'url'))
    clonedir  = os.path.expanduser(config.get(section, 'dir'))
    results[section] = retry(lambda: do_backup(remoteurl, clonedir),
                             config.getint('DEFAULT', 'max_attempts'),
                             config.getint('DEFAULT', 'backoff'))

with ResultsReporter(config.get('DEFAULT', 'notification')) as reporter:
    for section, result in results.iteritems():
        print >>reporter, result_to_s(section, result)
