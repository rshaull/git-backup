"""
Actions end in results, which are represented as Result objects.

Every Result object has a name field, which should be a short string
corresponding to the type of action performed, and a success field
which is True if the action succeeded, False otherwise. Finally, every
Result has a msg field which should be the error message if
success==False or else a descriptive message (which may be blank) if
success=True.
"""

import collections, time

Result = collections.namedtuple('Result', ['name', 'success', 'msg'])

def if_successful(result):
    return result if result.success else None

def if_failure(result):
    return result if not result.success else None

def retry(action, max_attempts, backoff):
    """
    Retry action until its result is successful, or until it has been
    attempted max_attempted times. For example, if max_attempts=2, then
    the action will be retried *once* after the first failure.

    action must be a function/lambda that takes no arguments.

    Waits backoff * N seconds before the Nth retry.

    Returns array of all results (result from last attempt will be last
    element in returned array).
    """
    attempts = []
    for attempt_num in xrange(max_attempts):
        attempts.append(action())
        if attempts[-1].success:
            # Done, don't need to run action again
            break
        elif attempt_num < max_attempts - 1:
            # Failure; wait before running action again
            time.sleep(backoff * attempt_num)
    return attempts

def result_to_s(section, result):
    """
    Returns a string describing the result, labeled by the section.

    If result is a list, assumes it is an array returned by retry
    (meaning the final attempt is the last result. Assumes every
    element in the array is a result and will use the last result in
    the array as the "succes/failure" determiner. If every result is
    a failure, will only print the failure message in the last result
    (although potentially every failure message *could* be useful).
    Will print the number of retries if more than one attempt was made.
    """
    if isinstance(result, list):
        num_retries = len(result) - 1
        result = result[-1]
    else:
        num_retries = 0
    retry_msg = "<%d retries>" % num_retries if num_retries > 0 else ""
    if result.success:
        return "[SUCCESS] %s: %s %s" % (section, result.name, retry_msg)
    return "[FAILURE] %s: %s error (%s) %s" % (section, result.name,
                                               result.msg, retry_msg)
