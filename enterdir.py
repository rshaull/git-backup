"Enter directories in a with block, optionally creating them."

import os

class EnterDir(object):
    """
    Context manager for entering a directory, optionally creating it
    and all intermediate directories on the specified path. Saves the
    current directory when the context is entered, and restores it
    when leaving the context.
    """
    __slots__ = ('path', 'create_if_not_exist', 'cwd')

    def __init__(self, path, create_if_not_exist=False):
        self.path = path
        self.create_if_not_exist = create_if_not_exist
    def __enter__(self):
        self.cwd = os.getcwd()
        if self.create_if_not_exist and not os.path.exists(self.path):
            os.makedirs(self.path)
        os.chdir(self.path)
    def __exit__(self, type, value, traceback):
        os.chdir(self.cwd)
